﻿using System;
using System.Diagnostics.CodeAnalysis;
using Features.Builder;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Features.Test
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class TimeFieldBuilderTests
    {
        #region Test Data

        public class TestFeature : IFeature
        {
            public DateTime DateTime { get; set; }
        }

        #endregion

        [TestCase]
        public void BuildTimePicker_FromDateTimeProperty_TimePickerShouldBeBuilt()
        {
            // Arrange
            var startTime = DateTime.Now;
            var featureBuilder = new FeatureBuilder<TestFeature>("test-feature");

            // Act
            var feature = featureBuilder
                .Presentation((presentationBuilder, testFeature) =>
                {
                    presentationBuilder.TimePicker(() => testFeature.DateTime, startTime)
                        .Title("DateTime-title")
                        .Description("DateTime-description")
                        .MinimumTime(Current.Time)
                        .MaximumTime(DateTime.Parse("12:00"));
                })
                .Build();

            // Result
            var value = feature.Value;
            value.Should().NotBeNull();
            value.DateTime.Should().Be(startTime);

            var presentation = feature.Presentation;
            AssertPresentation(presentation, "TimePicker");
            presentation["DateTime"]["@Constraints"].Should().HaveCount(2);
            presentation["DateTime"]["@Constraints"]["@MinimumTime"].Value<string>().Should().Be("Now");
            presentation["DateTime"]["@Constraints"]["@MaximumTime"].Value<string>().Should().Be("12:00");
        }

        [TestCase]
        public void BuildDatePicker_FromDateTimeProperty_DatePickerShouldBeBuilt()
        {
            // Arrange
            var startTime = DateTime.Now;
            var featureBuilder = new FeatureBuilder<TestFeature>("test-feature");

            // Act
            var feature = featureBuilder
                .Presentation((presentationBuilder, testFeature) =>
                {
                    presentationBuilder.DatePicker(() => testFeature.DateTime, startTime)
                        .Title("DateTime-title")
                        .Description("DateTime-description")
                        .MinimumDate(Current.Date)
                        .MaximumDate(DateTime.Parse("1.1.2016"));
                })
                .Build();

            // Result
            var value = feature.Value;
            value.Should().NotBeNull();
            value.DateTime.Should().Be(startTime);

            var presentation = feature.Presentation;
            AssertPresentation(presentation, "DatePicker");
            presentation["DateTime"]["@Constraints"].Should().HaveCount(2);
            presentation["DateTime"]["@Constraints"]["@MinimumDate"].Value<string>().Should().Be("Now");
            presentation["DateTime"]["@Constraints"]["@MaximumDate"].Value<string>().Should().Be("1.1.2016");
        }

        [TestCase]
        public void BuildDateTimePicker_FromDateTimeProperty_DateTimePickerShouldBeBuilt()
        {
            // Arrange
            var startTime = DateTime.Now;
            var featureBuilder = new FeatureBuilder<TestFeature>("test-feature");

            // Act
            var feature = featureBuilder
                .Presentation((presentationBuilder, testFeature) =>
                {
                    presentationBuilder.DateTimePicker(() => testFeature.DateTime, startTime)
                        .Title("DateTime-title")
                        .Description("DateTime-description")
                        .MinimumDate(Current.Date)
                        .MinimumTime(Current.Time)
                        .MaximumDate(DateTime.Parse("1.1.2016"))
                        .MaximumTime(DateTime.Parse("12:00"));
                })
                .Build();

            // Result
            var value = feature.Value;
            value.Should().NotBeNull();
            value.DateTime.Should().Be(startTime);

            var presentation = feature.Presentation;
            AssertPresentation(presentation, "DateTimePicker");
            presentation["DateTime"]["@Constraints"].Should().HaveCount(4);
            presentation["DateTime"]["@Constraints"]["@MinimumDate"].Value<string>().Should().Be("Now");
            presentation["DateTime"]["@Constraints"]["@MinimumTime"].Value<string>().Should().Be("Now");
            presentation["DateTime"]["@Constraints"]["@MaximumDate"].Value<string>().Should().Be("1.1.2016");
            presentation["DateTime"]["@Constraints"]["@MaximumTime"].Value<string>().Should().Be("12:00");
        }

        #region Helper Methods

        private static void AssertPresentation(Presentation presentation, string control)
        {
            presentation["DateTime"].Should().NotBeNull();
            presentation["DateTime"]["@Name"].Value<string>().Should().Be("DateTime");
            presentation["DateTime"]["@Title"].Value<string>().Should().Be("DateTime-title");
            presentation["DateTime"]["@Description"].Value<string>().Should().Be("DateTime-description");
            presentation["DateTime"]["@Type"].Value<string>().Should().Be("Time");
            presentation["DateTime"]["@Control"].Value<string>().Should().Be(control);
            presentation["DateTime"]["@Options"].Should().BeEmpty();
        }

        #endregion
    }
}