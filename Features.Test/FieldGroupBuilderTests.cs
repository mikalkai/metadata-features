﻿using System.Diagnostics.CodeAnalysis;
using Features.Builder;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Features.Test
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class FieldGroupBuilderTests
    {
        #region Test Data

        public class TestFeature : IFeature
        {
            public A Aa { get; set; }
        }

        public class A
        {
            public string Text { get; set; }

            public B Bb { get; set; }
        }

        public class B
        {
            public int Number { get; set; }
        }

        #endregion

        [TestCase]
        public void BuildFieldGroup_FromReferenceTypeProperty_ContainerFieldShouldBeBuilt()
        {
            // Arrange
            var featureBuilder = new FeatureBuilder<TestFeature>("test-feature");

            // Act
            var feature = featureBuilder
                .Presentation((presentationBuilder, testFeature) =>
                {
                    // Aa
                    presentationBuilder.FieldGroup(() => testFeature.Aa,
                        (aaBuilder, a) =>
                        {
                            // Aa.Text
                            aaBuilder.Title("Aa-title")
                                .Description("Aa-description")
                                .TextField(() => a.Text, "Aa.Text-value")
                                    .Title("Aa.Text-title")
                                    .Description("Aa.Text-description")
                                    .MaximumLength(10)
                                    .MinimumLength(1);

                            // Aa.Bb
                            aaBuilder.FieldGroup(() => a.Bb,
                                (bbBuilder, b) =>
                                {
                                    bbBuilder.Title("Aa.Bb-title")
                                        .Description("Aa.Bb-description")

                                        // Aa.Bb.Number
                                        .NumberField(() => b.Number, 11)
                                            .Title("Aa.Bb.Number-title")
                                            .Description("Aa.Bb.Number-description")
                                            .MaximumValue(9)
                                            .MinimumValue(2);
                                });
                        });
                })
                .Build();

            // Result
            var value = feature.Value;
            value.Should().NotBeNull();
            value.Aa.Should().NotBeNull();
            value.Aa.Text.Should().NotBeNull();
            value.Aa.Text.Should().Be("Aa.Text-value");
            value.Aa.Bb.Should().NotBeNull();
            value.Aa.Bb.Number.Should().Be(11);

            var presentation = feature.Presentation;
            presentation["Aa"].Should().NotBeNull();
            presentation["Aa"]["@Name"].Value<string>().Should().Be("Aa");
            presentation["Aa"]["@Title"].Value<string>().Should().Be("Aa-title");
            presentation["Aa"]["@Description"].Value<string>().Should().Be("Aa-description");
            presentation["Aa"]["@Type"].Value<string>().Should().Be("Object");
            presentation["Aa"]["@Control"].Value<string>().Should().Be("FieldGroup");
            presentation["Aa"]["@Options"].Should().BeEmpty();
            presentation["Aa"]["@Constraints"].Should().BeEmpty();

            presentation["Aa"]["Text"].Should().NotBeNull();
            presentation["Aa"]["Text"]["@Name"].Value<string>().Should().Be("Text");
            presentation["Aa"]["Text"]["@Title"].Value<string>().Should().Be("Aa.Text-title");
            presentation["Aa"]["Text"]["@Description"].Value<string>().Should().Be("Aa.Text-description");
            presentation["Aa"]["Text"]["@Type"].Value<string>().Should().Be("Text");
            presentation["Aa"]["Text"]["@Control"].Value<string>().Should().Be("TextBox");
            presentation["Aa"]["Text"]["@Options"].Should().BeEmpty();
            presentation["Aa"]["Text"]["@Constraints"].Should().HaveCount(2);
            presentation["Aa"]["Text"]["@Constraints"]["@MinimumLength"].Value<int>().Should().Be(1);
            presentation["Aa"]["Text"]["@Constraints"]["@MaximumLength"].Value<int>().Should().Be(10);

            presentation["Aa"]["Bb"].Should().NotBeNull();
            presentation["Aa"]["Bb"]["@Name"].Value<string>().Should().Be("Bb");
            presentation["Aa"]["Bb"]["@Title"].Value<string>().Should().Be("Aa.Bb-title");
            presentation["Aa"]["Bb"]["@Description"].Value<string>().Should().Be("Aa.Bb-description");
            presentation["Aa"]["Bb"]["@Type"].Value<string>().Should().Be("Object");
            presentation["Aa"]["Bb"]["@Control"].Value<string>().Should().Be("FieldGroup");
            presentation["Aa"]["Bb"]["@Options"].Should().BeEmpty();
            presentation["Aa"]["Bb"]["@Constraints"].Should().BeEmpty();

            presentation["Aa"]["Bb"]["Number"].Should().NotBeNull();
            presentation["Aa"]["Bb"]["Number"]["@Name"].Value<string>().Should().Be("Number");
            presentation["Aa"]["Bb"]["Number"]["@Title"].Value<string>().Should().Be("Aa.Bb.Number-title");
            presentation["Aa"]["Bb"]["Number"]["@Description"].Value<string>().Should().Be("Aa.Bb.Number-description");
            presentation["Aa"]["Bb"]["Number"]["@Type"].Value<string>().Should().Be("Number");
            presentation["Aa"]["Bb"]["Number"]["@Control"].Value<string>().Should().Be("TextBox");
            presentation["Aa"]["Bb"]["Number"]["@Options"].Should().BeEmpty();
            presentation["Aa"]["Bb"]["Number"]["@Constraints"]["@MinimumValue"].Value<int>().Should().Be(2);
            presentation["Aa"]["Bb"]["Number"]["@Constraints"]["@MaximumValue"].Value<int>().Should().Be(9);
        }
    }
}