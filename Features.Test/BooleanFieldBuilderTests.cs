﻿using System.Diagnostics.CodeAnalysis;
using Features.Builder;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Features.Test
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class BooleanFieldBuilderTests
    {
        #region Test Data

        public class TestFeature : IFeature
        {
            public bool Flag { get; set; }
        }

        #endregion

        [TestCase]
        public void BuildCheckBox_FromBoolProperty_CheckBoxShouldBeBuilt()
        {
            // Arrange
            var featureBuilder = new FeatureBuilder<TestFeature>("test-feature");

            // Act
            var feature = featureBuilder
                .Presentation((presentationBuilder, testFeature) =>
                {
                    presentationBuilder.CheckBox(() => testFeature.Flag, true)
                        .Title("Flag-title")
                        .Description("Flag-description");
                })
                .Build();

            // Result
            var value = feature.Value;
            value.Should().NotBeNull();
            value.Flag.Should().BeTrue();

            var presentation = feature.Presentation;
            AssertPresentation(presentation, "CheckBox");
        }

        [TestCase]
        public void BuildRadioButton_FromBoolProperty_RadioButtonShouldBeBuilt()
        {
            // Arrange
            var featureBuilder = new FeatureBuilder<TestFeature>("test-feature");

            // Act
            var feature = featureBuilder
                .Presentation((presentationBuilder, testFeature) =>
                {
                    presentationBuilder.RadioButton(() => testFeature.Flag, true)
                        .Title("Flag-title")
                        .Description("Flag-description");
                })
                .Build();

            // Result
            var value = feature.Value;
            value.Should().NotBeNull();
            value.Flag.Should().BeTrue();

            var presentation = feature.Presentation;
            AssertPresentation(presentation, "RadioButton");
        }

        #region Helper Methods

        private static void AssertPresentation(Presentation presentation, string control)
        {
            presentation["Flag"].Should().NotBeNull();
            presentation["Flag"]["@Name"].Value<string>().Should().Be("Flag");
            presentation["Flag"]["@Title"].Value<string>().Should().Be("Flag-title");
            presentation["Flag"]["@Description"].Value<string>().Should().Be("Flag-description");
            presentation["Flag"]["@Type"].Value<string>().Should().Be("Boolean");
            presentation["Flag"]["@Control"].Value<string>().Should().Be(control);
            presentation["Flag"]["@Options"].Should().BeEmpty();
            presentation["Flag"]["@Constraints"].Should().BeEmpty();
        }

        #endregion
    }
}