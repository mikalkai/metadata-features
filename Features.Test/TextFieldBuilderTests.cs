﻿using System.Diagnostics.CodeAnalysis;
using Features.Builder;
using FluentAssertions;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Features.Test
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class TextFieldBuilderTests
    {
        #region Test Data

        public class TestFeature : IFeature
        {
            public string Text { get; set; }
        }

        #endregion

        [Test]
        public void BuildTextField_FromStringProperty_TextFieldShouldBeBuilt()
        {
            // Arrange
            var featureBuilder = new FeatureBuilder<TestFeature>("test-feature");

            // Act
            var feature = featureBuilder
                .Presentation((presentationBuilder, testFeature) =>
                {
                    presentationBuilder.TextField(() => testFeature.Text, "Text-value")
                        .Title("Text-title")
                        .Description("Text-description")
                        .MinimumLength(1)
                        .MaximumLength(2);
                })
                .Build();

            // Result
            var value = feature.Value;
            value.Should().NotBeNull();
            value.Text.Should().Be("Text-value");

            var presentation = feature.Presentation;
            presentation["Text"].Should().NotBeNull();
            presentation["Text"]["@Name"].Value<string>().Should().Be("Text");
            presentation["Text"]["@Title"].Value<string>().Should().Be("Text-title");
            presentation["Text"]["@Description"].Value<string>().Should().Be("Text-description");
            presentation["Text"]["@Type"].Value<string>().Should().Be("Text");
            presentation["Text"]["@Control"].Value<string>().Should().Be("TextBox");
            presentation["Text"]["@Options"].Should().BeEmpty();
            presentation["Text"]["@Constraints"]["@MinimumLength"].Value<int>().Should().Be(1);
            presentation["Text"]["@Constraints"]["@MaximumLength"].Value<int>().Should().Be(2);
        }
    }
}