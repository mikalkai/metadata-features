﻿using System.Diagnostics.CodeAnalysis;
using Features.Builder;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Features.Test
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class NumberFieldBuilderTests
    {
        #region Test Data

        public class TestFeature : IFeature
        {
            public int Number { get; set; }
        }

        #endregion

        [Test]
        public void BuildNumberField_FromIntProperty_NumberFieldShouldBeBuilt()
        {
            // Arrange
            var featureBuilder = new FeatureBuilder<TestFeature>("test-feature");

            // Act
            var feature = featureBuilder
                .Presentation((presentationBuilder, testFeature) =>
                {
                    presentationBuilder.NumberField(() => testFeature.Number, 69)
                        .Title("Number-title")
                        .Description("Number-description")
                        .MinimumValue(1)
                        .MaximumValue(2);
                })
                .Build();

            // Result
            var value = feature.Value;
            value.Should().NotBeNull();
            value.Number.Should().Be(69);

            var presentation = feature.Presentation;
            presentation["Number"].Should().NotBeNull();
            presentation["Number"]["@Name"].Value<string>().Should().Be("Number");
            presentation["Number"]["@Title"].Value<string>().Should().Be("Number-title");
            presentation["Number"]["@Description"].Value<string>().Should().Be("Number-description");
            presentation["Number"]["@Type"].Value<string>().Should().Be("Number");
            presentation["Number"]["@Control"].Value<string>().Should().Be("TextBox");
            presentation["Number"]["@Options"].Should().BeEmpty();
            presentation["Number"]["@Constraints"]["@MinimumValue"].Value<int>().Should().Be(1);
            presentation["Number"]["@Constraints"]["@MaximumValue"].Value<int>().Should().Be(2);
        }
    }
}