﻿using System.Diagnostics.CodeAnalysis;
using Features.Builder;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

namespace Features.Test
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class FeatureBuilderTest
    {
        #region Test Data

        public class TestFeature : IFeature { }

        #endregion

        [TestCase]
        public void BuildTestFeature_ThatHasAllPropertiesSet_FeatureShouldBeBuilt()
        {
            // Arrange
            var featureBuilder = new FeatureBuilder<TestFeature>("test-feature");

            // Act
            var feature = featureBuilder.Title("TestFeature-title")
                .Description("TestFeature-description")
                .IsEnabled(true)
                .IsPrivate(true)
                .IsCustomizable(true)
                .Build();

            // Result
            feature.CSharpType.Should().Be(typeof(TestFeature).AssemblyQualifiedName);
            feature.Name.Should().Be("test-feature");
            feature.Title.Should().Be("TestFeature-title");
            feature.Description.Should().Be("TestFeature-description");
            feature.IsEnabled.Should().BeTrue();
            feature.IsPrivate.Should().BeTrue();
            feature.IsCustomizable.Should().BeTrue();
        }
    }
}