﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Features.Builder;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Features.Test
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class ListFieldBuilderTests
    {
        #region Test Data

        public class TestFeature : IFeature
        {
            public List<string> Texts { get; set; }

            public List<int> Numbers { get; set; }

            public List<A> Aas { get; set; }

            public List<B> Bbs { get; set; }
        }

        public class A
        {
            public string Text { get; set; }
        }

        public class B : ISelectableItem
        {
            public bool IsSelected { get; set; }

            public string Id { get; set; }
        }

        #endregion

        [TestCase]
        public void BuildListBox_FromListOfStringsProperty_ListBoxShouldBeBuilt()
        {
            // Arrange
            var featureBuilder = new FeatureBuilder<TestFeature>("test-feature");

            // Act
            var feature = featureBuilder
                .Presentation((presentationBuilder, testFeature) =>
                {
                    presentationBuilder.ListBox(() => testFeature.Texts, "a", "b", "c")
                        .Title("Texts-title")
                        .Description("Texts-description")
                        .SupportsAdd()
                        .SupportsRemove()
                        .TextFieldTemplate(templateBuilder =>
                        {
                            templateBuilder.Title("Texts-template-title")
                                .Description("Texts-template-description")
                                .MinimumLength(1)
                                .MaximumLength(2);
                        });
                })
                .Build();

            // Result
            var value = feature.Value;
            value.Should().NotBeNull();
            value.Texts.Should().HaveCount(3);
            value.Texts.Should().Contain("a");
            value.Texts.Should().Contain("b");
            value.Texts.Should().Contain("c");

            var presentation = feature.Presentation;
            presentation["Texts"].Should().NotBeNull();
            presentation["Texts"]["@Name"].Value<string>().Should().Be("Texts");
            presentation["Texts"]["@Title"].Value<string>().Should().Be("Texts-title");
            presentation["Texts"]["@Description"].Value<string>().Should().Be("Texts-description");
            presentation["Texts"]["@Type"].Value<string>().Should().Be("List");
            presentation["Texts"]["@Control"].Value<string>().Should().Be("ListBox");
            presentation["Texts"]["@Options"].Should().HaveCount(2);
            presentation["Texts"]["@Options"]["@SupportsAdd"].Value<bool>().Should().BeTrue();
            presentation["Texts"]["@Options"]["@SupportsRemove"].Value<bool>().Should().BeTrue();
            presentation["Texts"]["@Constraints"].Should().BeEmpty();
            presentation["Texts"]["@Extras"].Should().HaveCount(1);
            presentation["Texts"]["@Extras"]["@FieldTemplate"]["@Name"].Value<string>().Should().Be("Texts-template");
            presentation["Texts"]["@Extras"]["@FieldTemplate"]["@Title"].Value<string>().Should().Be("Texts-template-title");
            presentation["Texts"]["@Extras"]["@FieldTemplate"]["@Description"].Value<string>().Should().Be("Texts-template-description");
            presentation["Texts"]["@Extras"]["@FieldTemplate"]["@Type"].Value<string>().Should().Be("Text");
            presentation["Texts"]["@Extras"]["@FieldTemplate"]["@Control"].Value<string>().Should().Be("TextBox");
            presentation["Texts"]["@Extras"]["@FieldTemplate"]["@Options"].Should().BeEmpty();
            presentation["Texts"]["@Extras"]["@FieldTemplate"]["@Constraints"].Should().HaveCount(2);
            presentation["Texts"]["@Extras"]["@FieldTemplate"]["@Constraints"]["@MinimumLength"].Value<int>().Should().Be(1);
            presentation["Texts"]["@Extras"]["@FieldTemplate"]["@Constraints"]["@MaximumLength"].Value<int>().Should().Be(2);
        }

        [TestCase]
        public void BuildListBox_FromListOfIntsProperty_ListBoxShouldBeBuilt()
        {
            // Arrange
            var featureBuilder = new FeatureBuilder<TestFeature>("test-feature");

            // Act
            var feature = featureBuilder
                .Presentation((presentationBuilder, testFeature) =>
                {
                    presentationBuilder.ListBox(() => testFeature.Numbers, 1, 2, 3)
                        .Title("Numbers-title")
                        .Description("Numbers-description")
                        .SupportsAdd()
                        .SupportsRemove()
                        .NumberFieldTemplate(templateBuilder =>
                        {
                            templateBuilder.Title("Numbers-template-title")
                                .Description("Numbers-template-description")
                                .MinimumValue(1)
                                .MaximumValue(2);
                        });
                })
                .Build();

            // Result
            var value = feature.Value;
            value.Should().NotBeNull();
            value.Numbers.Should().HaveCount(3);
            value.Numbers.Should().Contain(1);
            value.Numbers.Should().Contain(2);
            value.Numbers.Should().Contain(3);

            var presentation = feature.Presentation;
            presentation["Numbers"].Should().NotBeNull();
            presentation["Numbers"]["@Name"].Value<string>().Should().Be("Numbers");
            presentation["Numbers"]["@Title"].Value<string>().Should().Be("Numbers-title");
            presentation["Numbers"]["@Description"].Value<string>().Should().Be("Numbers-description");
            presentation["Numbers"]["@Type"].Value<string>().Should().Be("List");
            presentation["Numbers"]["@Control"].Value<string>().Should().Be("ListBox");
            presentation["Numbers"]["@Options"].Should().HaveCount(2);
            presentation["Numbers"]["@Options"]["@SupportsAdd"].Value<bool>().Should().BeTrue();
            presentation["Numbers"]["@Options"]["@SupportsRemove"].Value<bool>().Should().BeTrue();
            presentation["Numbers"]["@Constraints"].Should().BeEmpty();
            presentation["Numbers"]["@Extras"].Should().HaveCount(1);
            presentation["Numbers"]["@Extras"]["@FieldTemplate"]["@Name"].Value<string>().Should().Be("Numbers-template");
            presentation["Numbers"]["@Extras"]["@FieldTemplate"]["@Title"].Value<string>().Should().Be("Numbers-template-title");
            presentation["Numbers"]["@Extras"]["@FieldTemplate"]["@Description"].Value<string>().Should().Be("Numbers-template-description");
            presentation["Numbers"]["@Extras"]["@FieldTemplate"]["@Type"].Value<string>().Should().Be("Number");
            presentation["Numbers"]["@Extras"]["@FieldTemplate"]["@Control"].Value<string>().Should().Be("TextBox");
            presentation["Numbers"]["@Extras"]["@FieldTemplate"]["@Options"].Should().BeEmpty();
            presentation["Numbers"]["@Extras"]["@FieldTemplate"]["@Constraints"].Should().HaveCount(2);
            presentation["Numbers"]["@Extras"]["@FieldTemplate"]["@Constraints"]["@MinimumValue"].Value<int>().Should().Be(1);
            presentation["Numbers"]["@Extras"]["@FieldTemplate"]["@Constraints"]["@MaximumValue"].Value<int>().Should().Be(2);
        }

        [TestCase]
        public void BuildListBox_FromListOfReferenceTypesProperty_ListBoxShouldBeBuilt()
        {
            // Arrange
            var featureBuilder = new FeatureBuilder<TestFeature>("test-feature");

            // Act
            var feature = featureBuilder
                .Presentation((presentationBuilder, testFeature) =>
                {
                    presentationBuilder.ListBox(() => testFeature.Aas, new A {Text = "1"}, new A {Text = "2"})
                        .Title("Aas-title")
                        .Description("Aas-description")
                        .SupportsAdd()
                        .SupportsRemove()
                        .FieldGroupTemplate((builder, a) =>
                        {
                            builder.Title("Aas-template-title")
                                .Description("Aas-template-description")
                                .TextField(() => a.Text)
                                    .Title("Aas-Text-template-title")
                                    .Description("Aas-Text-template-description");
                        });
                })
                .Build();

            // Result
            var value = feature.Value;
            value.Should().NotBeNull();
            value.Aas.Should().HaveCount(2);
            value.Aas.Should().Contain(a => a.Text.Equals("1"));
            value.Aas.Should().Contain(a => a.Text.Equals("2"));

            var presentation = feature.Presentation;
            presentation["Aas"].Should().NotBeNull();
            presentation["Aas"]["@Name"].Value<string>().Should().Be("Aas");
            presentation["Aas"]["@Title"].Value<string>().Should().Be("Aas-title");
            presentation["Aas"]["@Description"].Value<string>().Should().Be("Aas-description");
            presentation["Aas"]["@Type"].Value<string>().Should().Be("List");
            presentation["Aas"]["@Control"].Value<string>().Should().Be("ListBox");
            presentation["Aas"]["@Options"].Should().HaveCount(2);
            presentation["Aas"]["@Options"]["@SupportsAdd"].Value<bool>().Should().BeTrue();
            presentation["Aas"]["@Options"]["@SupportsRemove"].Value<bool>().Should().BeTrue();
            presentation["Aas"]["@Constraints"].Should().BeEmpty();
            presentation["Aas"]["@Extras"].Should().HaveCount(1);
            presentation["Aas"]["@Extras"]["@FieldTemplate"]["@Name"].Value<string>().Should().Be("Aas-template");
            presentation["Aas"]["@Extras"]["@FieldTemplate"]["@Title"].Value<string>().Should().Be("Aas-template-title");
            presentation["Aas"]["@Extras"]["@FieldTemplate"]["@Description"].Value<string>().Should().Be("Aas-template-description");
            presentation["Aas"]["@Extras"]["@FieldTemplate"]["@Type"].Value<string>().Should().Be("Object");
            presentation["Aas"]["@Extras"]["@FieldTemplate"]["@Control"].Value<string>().Should().Be("FieldGroup");
            presentation["Aas"]["@Extras"]["@FieldTemplate"]["@Options"].Should().BeEmpty();
            presentation["Aas"]["@Extras"]["@FieldTemplate"]["@Constraints"].Should().HaveCount(0);
            presentation["Aas"]["@Extras"]["@FieldTemplate"]["Text"].Should().NotBeNull();
            presentation["Aas"]["@Extras"]["@FieldTemplate"]["Text"]["@Name"].Value<string>().Should().Be("Text");
            presentation["Aas"]["@Extras"]["@FieldTemplate"]["Text"]["@Title"].Value<string>().Should().Be("Aas-Text-template-title");
            presentation["Aas"]["@Extras"]["@FieldTemplate"]["Text"]["@Description"].Value<string>().Should().Be("Aas-Text-template-description");
            presentation["Aas"]["@Extras"]["@FieldTemplate"]["Text"]["@Type"].Value<string>().Should().Be("Text");
            presentation["Aas"]["@Extras"]["@FieldTemplate"]["Text"]["@Control"].Value<string>().Should().Be("TextBox");
            presentation["Aas"]["@Extras"]["@FieldTemplate"]["Text"]["@Options"].Should().BeEmpty();
            presentation["Aas"]["@Extras"]["@FieldTemplate"]["Text"]["@Constraints"].Should().HaveCount(2);
            presentation["Aas"]["@Extras"]["@FieldTemplate"]["Text"]["@Constraints"]["@MinimumLength"].Value<string>().Should().BeNull();
            presentation["Aas"]["@Extras"]["@FieldTemplate"]["Text"]["@Constraints"]["@MaximumLength"].Value<string>().Should().BeNull();
        }

        [TestCase]
        public void BuildSelectionListBox_FromListOfSelectableItemsProperty_SelectionListBoxShouldBeBuilt()
        {
            // Arrange
            var featureBuilder = new FeatureBuilder<TestFeature>("test-feature");

            // Act
            var feature = featureBuilder
                .Presentation((presentationBuilder, testFeature) =>
                {
                    presentationBuilder.SelectionListBox(() => testFeature.Bbs, new B {Id = "Yes", IsSelected = true}, new B {Id = "No"})
                        .Title("Bbs-title")
                        .Description("Bbs-description")
                        .SupportsAdd()
                        .SupportsRemove()
                        .SelectableItemTemplate((builder, b) =>
                        {
                            builder.Title("Bbs-template-title")
                                .Description("Bbs-template-description")
                                .TextField(() => b.Id)
                                    .Title("Bbs-Id-template-title")
                                    .Description("Bbs-Id-template-description");
                        });
                })
                .Build();

            // Result
            var value = feature.Value;
            value.Should().NotBeNull();
            value.Bbs.Should().HaveCount(2);
            value.Bbs.Should().Contain(b => b.Id.Equals("Yes") && b.IsSelected);
            value.Bbs.Should().Contain(b => b.Id.Equals("No") && !b.IsSelected);

            var presentation = feature.Presentation;
            presentation["Bbs"].Should().NotBeNull();
            presentation["Bbs"]["@Name"].Value<string>().Should().Be("Bbs");
            presentation["Bbs"]["@Title"].Value<string>().Should().Be("Bbs-title");
            presentation["Bbs"]["@Description"].Value<string>().Should().Be("Bbs-description");
            presentation["Bbs"]["@Type"].Value<string>().Should().Be("List");
            presentation["Bbs"]["@Control"].Value<string>().Should().Be("SelectionListBox");
            presentation["Bbs"]["@Options"].Should().HaveCount(5);
            presentation["Bbs"]["@Options"]["@SupportsAdd"].Value<bool>().Should().BeTrue();
            presentation["Bbs"]["@Options"]["@SupportsRemove"].Value<bool>().Should().BeTrue();
            presentation["Bbs"]["@Options"]["@SupportsSelectOne"].Value<bool>().Should().BeTrue();
            presentation["Bbs"]["@Options"]["@SupportsSelectMany"].Value<bool>().Should().BeFalse();
            presentation["Bbs"]["@Options"]["@SupportsSelectAll"].Value<bool>().Should().BeFalse();
            presentation["Bbs"]["@Constraints"].Should().BeEmpty();
            presentation["Bbs"]["@Extras"].Should().HaveCount(2);
            presentation["Bbs"]["@Extras"]["@FieldTemplate"]["@Name"].Value<string>().Should().Be("Bbs-template");
            presentation["Bbs"]["@Extras"]["@FieldTemplate"]["@Title"].Value<string>().Should().Be("Bbs-template-title");
            presentation["Bbs"]["@Extras"]["@FieldTemplate"]["@Description"].Value<string>().Should().Be("Bbs-template-description");
            presentation["Bbs"]["@Extras"]["@FieldTemplate"]["@Type"].Value<string>().Should().Be("Object");
            presentation["Bbs"]["@Extras"]["@FieldTemplate"]["@Control"].Value<string>().Should().Be("FieldGroup");
            presentation["Bbs"]["@Extras"]["@FieldTemplate"]["@Options"].Should().BeEmpty();
            presentation["Bbs"]["@Extras"]["@FieldTemplate"]["@Constraints"].Should().HaveCount(0);
            presentation["Bbs"]["@Extras"]["@FieldTemplate"]["Id"].Should().NotBeNull();
            presentation["Bbs"]["@Extras"]["@FieldTemplate"]["Id"]["@Name"].Value<string>().Should().Be("Id");
            presentation["Bbs"]["@Extras"]["@FieldTemplate"]["Id"]["@Title"].Value<string>().Should().Be("Bbs-Id-template-title");
            presentation["Bbs"]["@Extras"]["@FieldTemplate"]["Id"]["@Description"].Value<string>().Should().Be("Bbs-Id-template-description");
            presentation["Bbs"]["@Extras"]["@FieldTemplate"]["Id"]["@Type"].Value<string>().Should().Be("Text");
            presentation["Bbs"]["@Extras"]["@FieldTemplate"]["Id"]["@Control"].Value<string>().Should().Be("TextBox");
            presentation["Bbs"]["@Extras"]["@FieldTemplate"]["Id"]["@Options"].Should().BeEmpty();
            presentation["Bbs"]["@Extras"]["@FieldTemplate"]["Id"]["@Constraints"].Should().HaveCount(2);
            presentation["Bbs"]["@Extras"]["@FieldTemplate"]["Id"]["@Constraints"]["@MinimumLength"].Value<string>().Should().BeNull();
            presentation["Bbs"]["@Extras"]["@FieldTemplate"]["Id"]["@Constraints"]["@MaximumLength"].Value<string>().Should().BeNull();
            presentation["Bbs"]["@Extras"]["@IsSelectedProperty"].Value<string>().Should().Be("IsSelected");
        }


        [TestCase(true, false)]
        public void BuildSelectionListBox_WhichSupportsSelectOne_SupportsSelectManyAndAllShouldBeFalse(
            bool supportsSelectMany,
            bool supportsSelectAll)
        {
            // Arrange
            var featureBuilder = new FeatureBuilder<TestFeature>("test-feature");

            // Act
            var feature = featureBuilder
                .Presentation((presentationBuilder, testFeature) =>
                {
                    var selectionListBoxBuilder = presentationBuilder.SelectionListBox(() => testFeature.Bbs,
                            new B {Id = "Yes", IsSelected = true},
                            new B {Id = "No"})
                        .Title("Bbs-title")
                        .Description("Bbs-description")
                        .SupportsAdd()
                        .SupportsRemove()
                        .SelectableItemTemplate((builder, b) =>
                        {
                            builder.Title("Bbs-template-title")
                                .Description("Bbs-template-description")
                                .TextField(() => b.Id)
                                .Title("Bbs-Id-template-title")
                                .Description("Bbs-Id-template-description");
                        });

                    if (supportsSelectMany)
                        selectionListBoxBuilder.SupportsSelectMany();

                    if (supportsSelectAll)
                        selectionListBoxBuilder.SupportsSelectAll();
                })
                .Build();

            // Result
            var presentation = feature.Presentation;
            presentation["Bbs"].Should().NotBeNull();
            presentation["Bbs"]["@Options"].Should().HaveCount(5);
            presentation["Bbs"]["@Options"]["@SupportsAdd"].Value<bool>().Should().BeTrue();
            presentation["Bbs"]["@Options"]["@SupportsRemove"].Value<bool>().Should().BeTrue();
            presentation["Bbs"]["@Options"]["@SupportsSelectOne"].Value<bool>().Should().Be(!supportsSelectMany);
            presentation["Bbs"]["@Options"]["@SupportsSelectMany"].Value<bool>().Should().Be(supportsSelectMany);
            presentation["Bbs"]["@Options"]["@SupportsSelectAll"].Value<bool>().Should().Be(supportsSelectAll);
        }
    }
}