# Metadata Features #

Idea is to decouple feature presentation from the actual feature data object.

## Feature Data ##

Feature data can be any class containing int, bool, string, DateTime, List (of ints, strings, bools, DateTimes or any reference types) or a reference type containing any of the preceding types. The only exception is a type which can be used along with selection list box. These needs to be extended from ISelectableItem interface. This ensures that all items that can be selected will contain IsSelected property where the client side updates the selection value.

## Presentation ##

Presentation contains instructions how to render the data. Presentation is a set of fields that can contain other fields. The presentation is constructed in the exact same JSON tree than the bind feature. The only difference is that field contains also reserved static properties which describes how the value should be presented on the client side. All reserved field properties are prefixed with '@' character.

*** Field Properties ***

Properties below are always present in every field:

* **Name** - The property name where the field is bind to.
* **Title** - Title for the field.
* **Description** - What does this value actually mean.
* **Type** - Value type of the bind property.
* **Control** - Control type used to render the field.
* **Options** - Possible options for the field, what does it support.
* **Constraints** - Restrictions for the field, like minimum and maximum value.
* **Extras** - Extra information about the field, like list item template.

*** Field Options ***

Properties below are available based on the property type:

* **SupportsAdd** - List box support adding a new item.
* **SupportsRemove** - List box supports removing existing item.
* **SupportsSelectOne** - List box supports single selection.
* **SupportsSelectAll** - List box supports select all function.
* **SupportsSelectMany** - List box support multi selection.

*** Field Constraints ***

Properties below are available based on the property type: 

* **MinimumLength** - Minimum allowed length for a string type.
* **MaximumLength** - Maximum allowed length for a string type.
* **MinimumValue** - Minimum allowed value for an int type
* **MaximumValue** - Maximum allowed value for an int type.
* **MinimumTime** - Minimum allowed time for a DateTime type.
* **MaximumTime** - Maximum allowed time for a DateTime type.
* **MinimumDate** - Minimum allowed date for a DateTime type.
* **MaximumDate** - Maximum allowed date for a DateTime type.

*** Field Extras ***

Properties below are available on a List<T> type:

* **FieldTemplate** - Template how to render items in a list.
* **IsSelectedProperty** - Name of the property where to store selection (true, false) in the actual data object.

## Text Field ##

Text field can be bind to a string property.

```
#!C#

public class TestFeature : IFeature
{
    public string Text { get; set; }
}

var feature = new FeatureBuilder<TestFeature>("test-feature")
    .Presentation((presentationBuilder, testFeature) =>
    {
        presentationBuilder.TextField(() => testFeature.Text, "Text-value")
            .Title("Text-title")
            .Description("Text-description")
            .MinimumLength(1)
            .MaximumLength(2);
    })
    .Build();
```

The above code creates the following JSON for 'test-feature' of TestFeature type

```
#!JSON

{
  "Text": "Text-value"
}
```

With he following presentation

```
#!JSON

{
  "Text": {
    "@Name": "Text",
    "@Title": "Text-title",
    "@Description": "Text-description",
    "@Type": "Text",
    "@Control": "TextBox",
    "@Options": {},
    "@Constraints": {
      "@MinimumLength": 1,
      "@MaximumLength": 2
    },
    "@Extras": {}
  }
}
```

## Number Field ##

Number field can be bind to a string property.

```
#!C#

public class TestFeature : IFeature
{
    public int Number { get; set; }
}

var feature = new FeatureBuilder<TestFeature>("test-feature")
    .Presentation((presentationBuilder, testFeature) =>
    {
        presentationBuilder.NumberField(() => testFeature.Number, 69)
            .Title("Number-title")
            .Description("Number-description")
            .MinimumValue(1)
            .MaximumValue(2);
    })
    .Build();
```

The above code creates the following JSON for 'test-feature' of TestFeature type

```
#!JSON

{
  "Number": 69
}
```

With he following presentation

```
#!JSON

{
  "Number": {
    "@Name": "Number",
    "@Title": "Number-title",
    "@Description": "Number-description",
    "@Type": "Number",
    "@Control": "TextBox",
    "@Options": {},
    "@Constraints": {
      "@MinimumValue": 1,
      "@MaximumValue": 2
    },
    "@Extras": {}
  }
}
```

## Check Box ##

Check box can be bind to a bool property.

```
#!C#

public class TestFeature : IFeature
{
    public bool Flag { get; set; }
}

var feature = new FeatureBuilder<TestFeature>("test-feature")
    .Presentation((presentationBuilder, testFeature) =>
    {
        presentationBuilder.CheckBox(() => testFeature.Flag, true)
            .Title("Flag-title")
            .Description("Flag-description");
    })
    .Build();
```

The above code creates the following JSON for 'test-feature' of TestFeature type

```
#!JSON

{
  "Flag": true
}
```

With he following presentation

```
#!JSON

{
  "Flag": {
    "@Name": "Flag",
    "@Title": "Flag-title",
    "@Description": "Flag-description",
    "@Type": "Boolean",
    "@Control": "CheckBox",
    "@Options": {},
    "@Constraints": {},
    "@Extras": {}
  }
}
```

## Radio Button ##

Radio button can be bind to a bool property.

```
#!C#

public class TestFeature : IFeature
{
    public bool Flag { get; set; }
}

var feature = new FeatureBuilder<TestFeature>("test-feature")
    .Presentation((presentationBuilder, testFeature) =>
    {
        presentationBuilder.RadioButton(() => testFeature.Flag, true)
            .Title("Flag-title")
            .Description("Flag-description");
    })
    .Build();
```

The above code creates the following JSON for 'test-feature' of TestFeature type

```
#!JSON

{
  "Flag":true
}
```

With he following presentation

```
#!JSON

{
  "Flag": {
    "@Name": "Flag",
    "@Title": "Flag-title",
    "@Description": "Flag-description",
    "@Type": "Boolean",
    "@Control": "RadioButton",
    "@Options": {},
    "@Constraints": {},
    "@Extras": {}
  }
}
```

## Time Picker ##

Time picker can be bind to a DateTime property.

```
#!C#

public class TestFeature : IFeature
{
    public DateTime DateTime { get; set; }
}

var feature = new FeatureBuilder<TestFeature>("test-feature")
    .Presentation((presentationBuilder, testFeature) =>
    {
        presentationBuilder.TimePicker(() => testFeature.DateTime, DateTime.Now)
            .Title("DateTime-title")
            .Description("DateTime-description")
            .MinimumTime(Current.Time)
            .MaximumTime(DateTime.Parse("12:00"));
    })
    .Build();
```

The above code creates the following JSON for 'test-feature' of TestFeature type

```
#!JSON

{
  "DateTime": "2016-09-17T21:45:20.8060154+03:00"
}
```

With he following presentation

```
#!JSON

{
  "DateTime": {
    "@Name": "DateTime",
    "@Title": "DateTime-title",
    "@Description": "DateTime-description",
    "@Type": "Time",
    "@Control": "TimePicker",
    "@Options": {},
    "@Constraints": {
      "@MinimumTime": "Now",
      "@MaximumTime": "12:00"
    },
    "@Extras": {}
  }
}
```

## Date Picker ##

Date picker can be bind to a DateTime property.

```
#!C#

public class TestFeature : IFeature
{
    public DateTime DateTime { get; set; }
}

var feature = new FeatureBuilder<TestFeature>("test-feature")
    .Presentation((presentationBuilder, testFeature) =>
    {
        presentationBuilder.TimePicker(() => testFeature.DateTime, DateTime.Now)
            .Title("DateTime-title")
            .Description("DateTime-description")
            .MinimumDate(Current.Date)
            .MaximumDate(DateTime.Parse("1.1.2016"));
    })
    .Build();
```

The above code creates the following JSON for 'test-feature' of TestFeature type

```
#!JSON

{
  "DateTime": "2016-09-17T21:45:20.8060154+03:00"
}
```

With he following presentation

```
#!JSON

{
  "DateTime": {
    "@Name": "DateTime",
    "@Title": "DateTime-title",
    "@Description": "DateTime-description",
    "@Type": "Time",
    "@Control": "DatePicker",
    "@Options": {},
    "@Constraints": {
      "@MinimumDate": "Now",
      "@MaximumDate": "12:00"
    },
    "@Extras": {}
  }
}
```

## Date Time Picker ##

DateTime picker can be bind to a DateTime property.

```
#!C#

public class TestFeature : IFeature
{
    public DateTime DateTime { get; set; }
}

var feature = new FeatureBuilder<TestFeature>("test-feature")
    .Presentation((presentationBuilder, testFeature) =>
    {
        presentationBuilder.TimePicker(() => testFeature.DateTime, DateTime.Now)
            .Title("DateTime-title")
            .Description("DateTime-description")
            .MinimumDate(Current.Date)
            .MinimumTime(Current.Time)
            .MaximumDate(DateTime.Parse("1.1.2016"))
            .MaximumTime(DateTime.Parse("12:00"));
    })
    .Build();
```

The above code creates the following JSON for 'test-feature' of TestFeature type

```
#!JSON

{
  "DateTime": "2016-09-17T21:45:20.8060154+03:00"
}
```

With he following presentation

```
#!JSON

{
  "DateTime": {
    "@Name": "DateTime",
    "@Title": "DateTime-title",
    "@Description": "DateTime-description",
    "@Type": "Time",
    "@Control": "DateTimePicker",
    "@Options": {},
    "@Constraints": {
      "@MinimumTime": "Now",
      "@MinimumDate": "Now",
      "@MaximumTime": "12:00",
      "@MaximumDate": "1.1.2016"
    },
    "@Extras": {}
  }
}
```

## Field Group ##

Field group can be bind to any reference type property.

```
#!C#

public class TestFeature : IFeature
{
    public A Aa { get; set; }
}

public class A
{
    public string Text { get; set; }

    public B Bb { get; set; }
}

public class B
{
    public int Number { get; set; }
}

var feature = new FeatureBuilder<TestFeature>("test-feature")
    .Presentation((presentationBuilder, testFeature) =>
    {
        // Aa
        presentationBuilder.FieldGroup(() => testFeature.Aa,
            (aaBuilder, a) =>
            {
                // Aa.Text
                aaBuilder.Title("Aa-title")
                    .Description("Aa-description")
                    .TextField(() => a.Text, "Aa.Text-value")
                        .Title("Aa.Text-title")
                        .Description("Aa.Text-description")
                        .MaximumLength(10)
                        .MinimumLength(1);

                // Aa.Bb
                aaBuilder.FieldGroup(() => a.Bb,
                    (bbBuilder, b) =>
                    {
                        bbBuilder.Title("Aa.Bb-title")
                            .Description("Aa.Bb-description")

                            // Aa.Bb.Number
                            .NumberField(() => b.Number, 11)
                                .Title("Aa.Bb.Number-title")
                                .Description("Aa.Bb.Number-description")
                                .MaximumValue(9)
                                .MinimumValue(2);
                    });
            });
    })
    .Build();
```

The above code creates the following JSON for 'test-feature' of TestFeature type

```
#!JSON

{
   "Aa": {
      "Text": "Aa.Text-value",
      "Bb": {
         "Number": 11
      }
   }
}
```

With he following presentation

```
#!JSON

{
  "Aa": {
    "@Name": "Aa",
    "@Title": "Aa-title",
    "@Description": "Aa-description",
    "@Type": "Object",
    "@Control": "FieldGroup",
    "@Options": {},
    "@Constraints": {},
    "@Extras": {},
    "Text": {
      "@Name": "Text",
      "@Title": "Aa.Text-title",
      "@Description": "Aa.Text-description",
      "@Type": "Text",
      "@Control": "TextBox",
      "@Options": {},
      "@Constraints": {
        "@MinimumLength": 1,
        "@MaximumLength": 10
      },
      "@Extras": {}
    },
    "Bb": {
      "@Name": "Bb",
      "@Title": "Aa.Bb-title",
      "@Description": "Aa.Bb-description",
      "@Type": "Object",
      "@Control": "FieldGroup",
      "@Options": {},
      "@Constraints": {},
      "@Extras": {},
      "Number": {
        "@Name": "Number",
        "@Title": "Aa.Bb.Number-title",
        "@Description": "Aa.Bb.Number-description",
        "@Type": "Number",
        "@Control": "TextBox",
        "@Options": {},
        "@Constraints": {
          "@MinimumValue": 2,
          "@MaximumValue": 9
        },
        "@Extras": {}
      }
    }
  }
}
```

## List Box ##

List box can be bind to any List<> property that is typed against int, bool, DateTime or any reference type.

### List Of String Types ###

```
#!C#

public class TestFeature : IFeature
{
    public List<string> Texts { get; set; }
}

var feature = new FeatureBuilder<TestFeature>("test-feature")
    .Presentation((presentationBuilder, testFeature) =>
    {
        presentationBuilder.ListBox(() => testFeature.Texts, "a", "b", "c")
            .Title("Texts-title")
            .Description("Texts-description")
            .SupportsAdd()
            .SupportsRemove()
            .TextFieldTemplate(templateBuilder =>
            {
                templateBuilder.Title("Texts-template-title")
                    .Description("Texts-template-description")
                    .MinimumLength(1)
                    .MaximumLength(2);
            });
    })
    .Build();
```

The above code creates the following JSON for 'test-feature' of TestFeature type

```
#!JSON

{
   "Texts": [
      "a",
      "b",
      "c"
   ],
}
```

With he following presentation

```
#!JSON

{
  "Texts": {
    "@Name": "Texts",
    "@Title": "Texts-title",
    "@Description": "Texts-description",
    "@Type": "List",
    "@Control": "ListBox",
    "@Options": {
      "@SupportsAdd": true,
      "@SupportsRemove": true
    },
    "@Constraints": {},
    "@Extras": {
      "@FieldTemplate": {
        "@Name": "Texts-template",
        "@Title": "Texts-template-title",
        "@Description": "Texts-template-description",
        "@Type": "Text",
        "@Control": "TextBox",
        "@Options": {},
        "@Constraints": {
          "@MinimumLength": 1,
          "@MaximumLength": 2
        },
        "@Extras": {}
      }
    }
  }
}
```


### List Of Int Types ###

```
#!C#

public class TestFeature : IFeature
{
    public List<int> Numbers { get; set; }
}

var feature = new FeatureBuilder<TestFeature>("test-feature")
    .Presentation((presentationBuilder, testFeature) =>
    {
        presentationBuilder.ListBox(() => testFeature.Numbers, 1, 2, 3)
            .Title("Numbers-title")
            .Description("Numbers-description")
            .SupportsAdd()
            .SupportsRemove()
            .NumberFieldTemplate(templateBuilder =>
            {
                templateBuilder.Title("Numbers-template-title")
                    .Description("Numbers-template-description")
                    .MinimumValue(1)
                    .MaximumValue(2);
            });
    })
    .Build();
```

The above code creates the following JSON for 'test-feature' of TestFeature type

```
#!JSON

{
   "Numbers": [
      1,
      2,
      3
   ],
}
```

With he following presentation

```
#!JSON

{
  "Numbers": {
    "@Name": "Numbers",
    "@Title": "Numbers-title",
    "@Description": "Numbers-description",
    "@Type": "List",
    "@Control": "ListBox",
    "@Options": {
      "@SupportsAdd": true,
      "@SupportsRemove": true
    },
    "@Constraints": {},
    "@Extras": {
      "@FieldTemplate": {
        "@Name": "Numbers-template",
        "@Title": "Numbers-template-title",
        "@Description": "Numbers-template-description",
        "@Type": "Number",
        "@Control": "TextBox",
        "@Options": {},
        "@Constraints": {
          "@MinimumValue": 1,
          "@MaximumValue": 2
        },
        "@Extras": {}
      }
    }
  }
}
```

### List Of Reference Types ###

```
#!C#

public class TestFeature : IFeature
{
    public List<A> Aas { get; set; }
}

public class A
{
    public string Text { get; set; }
}

var feature = new FeatureBuilder<TestFeature>("test-feature")
    .Presentation((presentationBuilder, testFeature) =>
    {
        presentationBuilder.ListBox(() => testFeature.Aas, new A {Text = "1"}, new A {Text = "2"})
            .Title("Aas-title")
            .Description("Aas-description")
            .SupportsAdd()
            .SupportsRemove()
            .FieldGroupTemplate((builder, a) =>
            {
                builder.Title("Aas-template-title")
                    .Description("Aas-template-description")
                    .TextField(() => a.Text)
                        .Title("Aas-Text-template-title")
                        .Description("Aas-Text-template-description");
            });
    })
    .Build();
```

The above code creates the following JSON for 'test-feature' of TestFeature type

```
#!JSON

{
  "Aas": [
     {
        "Text": "1"
     },
     {
        "Text": "2"
     }
  ],
}
```

With he following presentation

```
#!JSON

{
  "Aas": {
    "@Name": "Aas",
    "@Title": "Aas-title",
    "@Description": "Aas-description",
    "@Type": "List",
    "@Control": "ListBox",
    "@Options": {
      "@SupportsAdd": true,
      "@SupportsRemove": true
    },
    "@Constraints": {},
    "@Extras": {
      "@FieldTemplate": {
        "@Name": "Aas-template",
        "@Title": "Aas-template-title",
        "@Description": "Aas-template-description",
        "@Type": "Object",
        "@Control": "FieldGroup",
        "@Options": {},
        "@Constraints": {},
        "@Extras": {},
        "Text": {
          "@Name": "Text",
          "@Title": "Aas-Text-template-title",
          "@Description": "Aas-Text-template-description",
          "@Type": "Text",
          "@Control": "TextBox",
          "@Options": {},
          "@Constraints": {
            "@MinimumLength": null,
            "@MaximumLength": null
          },
          "@Extras": {}
        }
      }
    }
  }
}
```

## Selection List Box ##

Selection list box can be bind to any reference type extending ISelectableItem.

```
#!C#

public class TestFeature : IFeature
{
    public List<B> Bbs { get; set; }
}

public class B : ISelectableItem
{
    public bool IsSelected { get; set; }

    public string Id { get; set; }
}

var feature = new FeatureBuilder<TestFeature>("test-feature")
    .Presentation((presentationBuilder, testFeature) =>
    {
        presentationBuilder.SelectionListBox(() => testFeature.Bbs, new B {Id = "Yes", IsSelected = true}, new B {Id = "No"})
            .Title("Bbs-title")
            .Description("Bbs-description")
            .SupportsAdd()
            .SupportsRemove()
            .SelectableItemTemplate((builder, b) =>
            {
                builder.Title("Bbs-template-title")
                    .Description("Bbs-template-description")
                    .TextField(() => b.Id)
                        .Title("Bbs-Id-template-title")
                        .Description("Bbs-Id-template-description");
            });
    })
    .Build();
```

The above code creates the following JSON for 'test-feature' of TestFeature type

```
#!JSON

{
  "Bbs":[
    {
      "IsSelected":true,
      "Id":"Yes"
    },
    {
      "IsSelected":false,
      "Id":"No"
    }
  ]
}
```

With he following presentation

```
#!JSON

{
  "Bbs": {
    "@Name": "Bbs",
    "@Title": "Bbs-title",
    "@Description": "Bbs-description",
    "@Type": "List",
    "@Control": "SelectionListBox",
    "@Options": {
      "@SupportsAdd": true,
      "@SupportsRemove": true,
      "@SupportsSelectOne": true,
      "@SupportsSelectAll": false,
      "@SupportsSelectMany": false
    },
    "@Constraints": {},
    "@Extras": {
      "@IsSelectedProperty": "IsSelected",
      "@FieldTemplate": {
        "@Name": "Bbs-template",
        "@Title": "Bbs-template-title",
        "@Description": "Bbs-template-description",
        "@Type": "Object",
        "@Control": "FieldGroup",
        "@Options": {},
        "@Constraints": {},
        "@Extras": {},
        "Id": {
          "@Name": "Id",
          "@Title": "Bbs-Id-template-title",
          "@Description": "Bbs-Id-template-description",
          "@Type": "Text",
          "@Control": "TextBox",
          "@Options": {},
          "@Constraints": {
            "@MinimumLength": null,
            "@MaximumLength": null
          },
          "@Extras": {}
        }
      }
    }
  }
}
```