﻿namespace Features
{
    public enum FieldOption
    {
        /// <summary>
        /// Options available for List <see cref="PropertyType"/> type.
        /// </summary>
        SupportsSelectAll,
        SupportsSelectOne,
        SupportsSelectMany,
        SupportsAdd,
        SupportsRemove
    }
}