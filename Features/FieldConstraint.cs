﻿namespace Features
{
    public enum FieldConstraint
    {
        /// <summary>
        /// Common constraints.
        /// </summary>
        ReadOnly,

        /// <summary>
        /// Constraints for the Text <see cref="PropertyType"/> type.
        /// </summary>
        MinimumLength,
        MaximumLength,

        /// <summary>
        /// Constraints for the Number <see cref="PropertyType"/> type.
        /// </summary>
        MinimumValue,
        MaximumValue,

        /// <summary>
        /// Constraints for the Time <see cref="PropertyType"/> type.
        /// </summary>
        MinimumTime,
        MinimumDate,
        MaximumTime,
        MaximumDate
    }
}