﻿namespace Features
{
    public class Feature<T>
    {
        public Feature(string name,
            string title,
            string description,
            bool isEnabled,
            bool isPrivate,
            bool isCustomizable,
            T value,
            Presentation presentation)
        {
            CSharpType = typeof(T).AssemblyQualifiedName;
            Name = name;
            Title = title;
            Description = description;
            IsEnabled = isEnabled;
            IsPrivate = isPrivate;
            IsCustomizable = isCustomizable;
            Value = value;
            Presentation = presentation;
        }

        public string CSharpType { get; }

        public string Name { get; }

        public string Title { get; }

        public string Description { get; }

        public bool IsEnabled { get; }

        public bool IsPrivate { get; }

        public bool IsCustomizable { get; }

        public T Value { get; }

        public Presentation Presentation { get; }
    }
}