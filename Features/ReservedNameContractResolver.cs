﻿using System;
using System.Linq;
using Newtonsoft.Json.Serialization;

namespace Features
{
    public class ReservedNameContractResolver : DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            return Enum.GetNames(typeof(FieldConstraint)).Any(name => name.Equals(propertyName))
                ? $"@{propertyName}"
                : Enum.GetNames(typeof(FieldExtra)).Any(name => name.Equals(propertyName))
                    ? $"@{propertyName}"
                    : Enum.GetNames(typeof(FieldOption)).Any(name => name.Equals(propertyName))
                        ? $"@{propertyName}"
                        : propertyName;
        }
    }
}