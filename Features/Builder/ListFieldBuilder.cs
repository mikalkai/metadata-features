﻿using System;
using System.Collections.Generic;

namespace Features.Builder
{
    public class ListFieldBuilder<T1, T2> : FieldBuilder<T1, List<T2>> where T1 : ListFieldBuilder<T1, T2>
    {
        internal FieldBuilder FieldTemplateBuilder;

        protected ListFieldBuilder(string propertyName, List<T2> propertyValue, ControlType controlType)
            : base(propertyName, propertyValue, PropertyType.List, controlType)
        {
            Options[FieldOption.SupportsAdd] = false;
            Options[FieldOption.SupportsRemove] = false;
        }

        public T1 SupportsAdd()
        {
            Options[FieldOption.SupportsAdd] = true;
            return (T1) this;
        }

        public T1 SupportsRemove()
        {
            Options[FieldOption.SupportsRemove] = true;
            return (T1) this;
        }

        public override Field Build(Action<string, object> action)
        {
            if (FieldTemplateBuilder == null)
                throw new PropertyTemplateMissingException(PropertyName);

            Extras[FieldExtra.FieldTemplate] = FieldTemplateBuilder.Build((s, o) => { });
            return base.Build(action);
        }
    }
}