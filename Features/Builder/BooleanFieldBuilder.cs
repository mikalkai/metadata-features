﻿namespace Features.Builder
{
    public class BooleanFieldBuilder : FieldBuilder<BooleanFieldBuilder, bool?>
    {
        protected BooleanFieldBuilder(string propertyName, bool? propertyValue, ControlType controlType)
            : base(propertyName, propertyValue, PropertyType.Boolean, controlType)
        {
        }
    }
}