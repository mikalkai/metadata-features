using System;
using System.Collections.Generic;

namespace Features.Builder
{
    public class ListBoxBuilder<T> : ListFieldBuilder<ListBoxBuilder<T>, T>
    {
        public ListBoxBuilder(string propertyName, List<T> propertyValue)
            : base(propertyName, propertyValue, ControlType.ListBox)
        {
        }
    }

    public static class ListBoxBuilder
    {
        public static ListBoxBuilder<string> TextFieldTemplate(this ListBoxBuilder<string> builder,
            Action<TextFieldBuilder> action, string defaultValue = null)
        {
            var templateBuilder = new TextFieldBuilder($"{builder.PropertyName}-template", defaultValue);
            action(templateBuilder);
            builder.FieldTemplateBuilder = templateBuilder;
            return builder;
        }

        public static ListBoxBuilder<int> NumberFieldTemplate(this ListBoxBuilder<int> builder,
            Action<NumberFieldBuilder> action, int? defaultValue = null)
        {
            var templateBuilder = new NumberFieldBuilder($"{builder.PropertyName}-template", defaultValue);
            action(templateBuilder);
            builder.FieldTemplateBuilder = templateBuilder;
            return builder;
        }

        public static ListBoxBuilder<T> FieldGroupTemplate<T>(this ListBoxBuilder<T> builder, Action<FieldGroupBuilder<T>, T> action) 
            where T : class, new()
        {
            var t = new T();
            var templateBuilder = new FieldGroupBuilder<T>($"{builder.PropertyName}-template", t);
            action(templateBuilder, t);
            builder.FieldTemplateBuilder = templateBuilder;
            return builder;
        }
    }
}
