﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Features.Builder
{
    public class PresentationBuilder<TFeature> where TFeature : IFeature
    {
        private readonly List<FieldBuilder> _builders = new List<FieldBuilder>();            
        private readonly TFeature _feature;

        public PresentationBuilder(TFeature feature)
        {
            _feature = feature;
        }

        public TextFieldBuilder TextField(Expression<Func<string>> expression, string value = null)
        {
            var builder = new TextFieldBuilder(expression.GetPropertyName(), value);
            _builders.Add(builder);
            return builder;
        }

        public NumberFieldBuilder NumberField(Expression<Func<int>> expression, int? value = null)
        {
            var builder = new NumberFieldBuilder(expression.GetPropertyName(), value);
            _builders.Add(builder);
            return builder;
        }

        public CheckBoxFieldBuilder CheckBox(Expression<Func<bool>> expression, bool? value = null)
        {
            var builder = new CheckBoxFieldBuilder(expression.GetPropertyName(), value);
            _builders.Add(builder);
            return builder;
        }

        public RadioButtonFieldBuilder RadioButton(Expression<Func<bool>> expression, bool? value = null)
        {
            var builder = new RadioButtonFieldBuilder(expression.GetPropertyName(), value);
            _builders.Add(builder);
            return builder;
        }

        public TimePickerBuilder TimePicker(Expression<Func<DateTime>> expression, DateTime? value = null)
        {
            var builder = new TimePickerBuilder(expression.GetPropertyName(), value);
            _builders.Add(builder);
            return builder;
        }

        public DatePickerBuilder DatePicker(Expression<Func<DateTime>> expression, DateTime? value = null)
        {
            var builder = new DatePickerBuilder(expression.GetPropertyName(), value);
            _builders.Add(builder);
            return builder;
        }

        public DateTimePickerBuilder DateTimePicker(Expression<Func<DateTime>> expression, DateTime? value = null)
        {
            var builder = new DateTimePickerBuilder(expression.GetPropertyName(), value);
            _builders.Add(builder);
            return builder;
        }

        public ListBoxBuilder<T> ListBox<T>(Expression<Func<List<T>>> expression, params T[] values)
        {
            var builder = new ListBoxBuilder<T>(expression.GetPropertyName(), values.ToList());
            _builders.Add(builder);
            return builder;
        }

        public SelectionListBoxBuilder<T> SelectionListBox<T>(Expression<Func<List<T>>> expression, params T[] values)
            where T : class, ISelectableItem, new()
        {
            var builder = new SelectionListBoxBuilder<T>(expression.GetPropertyName(), values.ToList());
            _builders.Add(builder);
            return builder;
        }

        public void FieldGroup<T>(Expression<Func<T>> expression, Action<FieldGroupBuilder<T>, T> action) 
            where T : class, new()
        {
            var propertyValue = new T();
            var builder = new FieldGroupBuilder<T>(expression.GetPropertyName(), propertyValue);
            _builders.Add(builder);
            action(builder, propertyValue);
        }

        public Presentation Build()
        {
            var fields = new List<Field>();
            foreach (var builder in _builders)
            {
                var field = builder.Build((name, value) =>
                {
                    var propertyInfo = _feature.GetType().GetProperties().FirstOrDefault(info => info.Name.Equals(name));
                    if (propertyInfo == null)
                    {
                        throw new PropertyNotFoundException(name, _feature.GetType());
                    }
                    propertyInfo.SetValue(_feature, value);
                });
                fields.Add(field);
            }
            return new Presentation(fields);
        }
    }
}