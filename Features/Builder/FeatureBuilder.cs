﻿using System;

namespace Features.Builder
{
    public class FeatureBuilder<T> where T : IFeature, new()
    {
        private readonly string _name;
        private readonly T _value = new T();
        private string _title;
        private string _description;
        private bool _isEnabled;
        private bool _isPrivate;
        private PresentationBuilder<T> _presentationBuilder;
        private bool _isCustomizable;

        public FeatureBuilder(string name)
        {
            _name = name;
        }

        public FeatureBuilder<T> Title(string title)
        {
            _title = title;
            return this;
        }

        public FeatureBuilder<T> Description(string description)
        {
            _description = description;
            return this;
        }

        public FeatureBuilder<T> IsEnabled(bool isEnabled)
        {
            _isEnabled = isEnabled;
            return this;
        }

        public FeatureBuilder<T> IsPrivate(bool isPrivate)
        {
            _isPrivate = isPrivate;
            return this;
        }

        public FeatureBuilder<T> IsCustomizable(bool isCustomizable)
        {
            _isCustomizable = isCustomizable;
            return this;
        }

        public FeatureBuilder<T> Presentation(Action<PresentationBuilder<T>, T> action)
        {
            _presentationBuilder = new PresentationBuilder<T>(_value);
            action(_presentationBuilder, _value);
            return this;
        }

        public Feature<T> Build()
        {
            return new Feature<T>(_name,
                _title,
                _description,
                _isEnabled,
                _isPrivate,
                _isCustomizable,
                _value,
                _presentationBuilder?.Build());
        }
    }
}