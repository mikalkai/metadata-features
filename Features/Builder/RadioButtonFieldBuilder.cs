namespace Features.Builder
{
    public class RadioButtonFieldBuilder : BooleanFieldBuilder
    {
        public RadioButtonFieldBuilder(string propertyName, bool? propertyValue)
            : base(propertyName, propertyValue, ControlType.RadioButton)
        {
        }
    }
}