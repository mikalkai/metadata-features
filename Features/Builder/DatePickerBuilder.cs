using System;

namespace Features.Builder
{
    public class DatePickerBuilder : TimeFieldBuilder<DatePickerBuilder>
    {
        public DatePickerBuilder(string propertyName, DateTime? propertyValue)
            : base(propertyName, propertyValue, ControlType.DatePicker)
        {
            Constraints[FieldConstraint.MinimumDate] = null;
            Constraints[FieldConstraint.MaximumDate] = null;
        }

        public DatePickerBuilder MinimumDate(DateTime minimumDate)
        {
            Constraints[FieldConstraint.MinimumDate] = NowOrDate(minimumDate);
            return this;
        }

        public DatePickerBuilder MaximumDate(DateTime maximumDate)
        {
            Constraints[FieldConstraint.MaximumDate] = NowOrDate(maximumDate);
            return this;
        }
    }
}