﻿namespace Features.Builder
{
    public class NumberFieldBuilder : FieldBuilder<NumberFieldBuilder, int?>
    {
        public NumberFieldBuilder(string propertyName, int? propertyValue)
            : base(propertyName, propertyValue, PropertyType.Number, ControlType.TextBox)
        {
            Constraints[FieldConstraint.MinimumValue] = null;
            Constraints[FieldConstraint.MaximumValue] = null;
        }

        public NumberFieldBuilder MinimumValue(int minimumValue)
        {
            Constraints[FieldConstraint.MinimumValue] = minimumValue;
            return this;
        }

        public NumberFieldBuilder MaximumValue(int maximumValue)
        {
            Constraints[FieldConstraint.MaximumValue] = maximumValue;
            return this;
        }
    }
}