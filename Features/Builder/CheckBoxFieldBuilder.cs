namespace Features.Builder
{
    public class CheckBoxFieldBuilder : BooleanFieldBuilder
    {
        public CheckBoxFieldBuilder(string propertyName, bool? propertyValue)
            : base(propertyName, propertyValue, ControlType.CheckBox)
        {
        }
    }
}