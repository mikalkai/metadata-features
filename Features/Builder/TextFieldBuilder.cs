﻿namespace Features.Builder
{
    public class TextFieldBuilder : FieldBuilder<TextFieldBuilder, string>
    {
        public TextFieldBuilder(string propertyName, string propertyValue)
            : base(propertyName, propertyValue, PropertyType.Text, ControlType.TextBox)
        {
            Constraints[FieldConstraint.MinimumLength] = null;
            Constraints[FieldConstraint.MaximumLength] = null;
        }

        public TextFieldBuilder MinimumLength(int length)
        {
            Constraints[FieldConstraint.MinimumLength] = length;
            return this;
        }

        public TextFieldBuilder MaximumLength(int length)
        {
            Constraints[FieldConstraint.MaximumLength] = length;
            return this;
        }
    }
}