﻿using System;

namespace Features.Builder
{
    public static class Current
    {
        public static readonly DateTime Time = DateTime.Now;

        public static readonly DateTime Date = DateTime.Now;
    }

    public class TimeFieldBuilder<T> : FieldBuilder<T, DateTime?> where T : TimeFieldBuilder<T>
    {
        protected TimeFieldBuilder(string propertyName, DateTime? propertyValue, ControlType controlType)
            : base(propertyName, propertyValue, PropertyType.Time, controlType)
        {
        }

        protected static object NowOrTime(DateTime? dateTime)
        {
            return dateTime == Current.Time ? "Now" : dateTime?.ToShortTimeString();
        }

        protected static object NowOrDate(DateTime? dateTime)
        {
            return dateTime == Current.Date ? "Now" : dateTime?.ToShortDateString();
        }
    }
}