﻿using System;

namespace Features.Builder
{
    public class TimePickerBuilder : TimeFieldBuilder<TimePickerBuilder>
    {
        public TimePickerBuilder(string propertyName, DateTime? propertyValue)
            : base(propertyName, propertyValue, ControlType.TimePicker)
        {
            Constraints[FieldConstraint.MinimumTime] = null;
            Constraints[FieldConstraint.MaximumTime] = null;
        }

        public TimePickerBuilder MinimumTime(DateTime minimumTime)
        {
            Constraints[FieldConstraint.MinimumTime] = NowOrTime(minimumTime);
            return this;
        }

        public TimePickerBuilder MaximumTime(DateTime maximumTime)
        {
            Constraints[FieldConstraint.MaximumTime] = NowOrTime(maximumTime);
            return this;
        }
    }
}