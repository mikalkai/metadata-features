﻿using System;

namespace Features.Builder
{
    public class PropertyTemplateMissingException : Exception
    {
        public PropertyTemplateMissingException(string propertyName)
            : base($"Property template is missing from a ListProperty with name '{propertyName}'.")
        {
        }
    }
}