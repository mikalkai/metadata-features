﻿using System;
using System.Collections.Generic;

namespace Features.Builder
{
    public class SelectionListBoxBuilder<T> : ListFieldBuilder<SelectionListBoxBuilder<T>, T>
        where T : class, ISelectableItem, new()
    {
        public SelectionListBoxBuilder(string propertyName, List<T> propertyValue)
            : base(propertyName, propertyValue, ControlType.SelectionListBox)
        {
            Options[FieldOption.SupportsSelectOne] = true;
            Options[FieldOption.SupportsSelectAll] = false;
            Options[FieldOption.SupportsSelectMany] = false;
        }

        public void SupportsSelectAll()
        {
            Options[FieldOption.SupportsSelectOne] = false;
            Options[FieldOption.SupportsSelectMany] = true;
            Options[FieldOption.SupportsSelectAll] = true;
        }

        public void SupportsSelectOne()
        {
            Options[FieldOption.SupportsSelectOne] = true;
            Options[FieldOption.SupportsSelectMany] = false;
            Options[FieldOption.SupportsSelectAll] = false;
        }

        public void SupportsSelectMany()
        {
            Options[FieldOption.SupportsSelectOne] = false;
            Options[FieldOption.SupportsSelectMany] = true;
        }

        public override Field Build(Action<string, object> action)
        {
            Extras[FieldExtra.IsSelectedProperty] = IsSelectedPropertyName();
            return base.Build(action);
        }

        private string IsSelectedPropertyName()
        {
            // ReSharper disable once RedundantAssignment
            var t = new T();
            return nameof(t.IsSelected);
        }
    }

    public static class SelectionListBoxBuilder
    {
        public static SelectionListBoxBuilder<T> SelectableItemTemplate<T>(this SelectionListBoxBuilder<T> builder, Action<FieldGroupBuilder<T>, T> action) 
            where T : class, ISelectableItem, new()
        {
            var t = new T();
            var templateBuilder = new FieldGroupBuilder<T>($"{builder.PropertyName}-template", t);
            action(templateBuilder, t);
            builder.FieldTemplateBuilder = templateBuilder;
            return builder;
        }
    }
}