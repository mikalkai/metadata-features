﻿using System;

namespace Features.Builder
{
    public class DateTimePickerBuilder : TimeFieldBuilder<DateTimePickerBuilder>
    {
        public DateTimePickerBuilder(string propertyName, DateTime? propertyValue)
            : base(propertyName, propertyValue, ControlType.DateTimePicker)
        {
            Constraints[FieldConstraint.MinimumTime] = null;
            Constraints[FieldConstraint.MinimumDate] = null;
            Constraints[FieldConstraint.MaximumTime] = null;
            Constraints[FieldConstraint.MaximumDate] = null;
        }

        public DateTimePickerBuilder MinimumTime(DateTime minimumTime)
        {
            Constraints[FieldConstraint.MinimumTime] = NowOrTime(minimumTime);
            return this;
        }

        public DateTimePickerBuilder MinimumDate(DateTime minimumDate)
        {
            Constraints[FieldConstraint.MinimumDate] = NowOrDate(minimumDate);
            return this;
        }

        public DateTimePickerBuilder MaximumTime(DateTime maximumTime)
        {
            Constraints[FieldConstraint.MaximumTime] = NowOrTime(maximumTime);
            return this;
        }

        public DateTimePickerBuilder MaximumDate(DateTime maximumDate)
        {
            Constraints[FieldConstraint.MaximumDate] = NowOrDate(maximumDate);
            return this;
        }
    }
}