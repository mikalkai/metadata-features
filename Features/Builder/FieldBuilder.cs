﻿using System;
using System.Collections.Generic;

namespace Features.Builder
{
    public abstract class FieldBuilder
    {
        public abstract Field Build(Action<string, object> action);
    }

    public abstract class FieldBuilder<T1, T2> : FieldBuilder
        where T1 : FieldBuilder<T1, T2>
    {
        internal string PropertyName { get; }
        internal T2 PropertyValue { get; }

        protected Dictionary<FieldOption, object> Options { get; }
        protected Dictionary<FieldConstraint, object> Constraints { get; }
        protected Dictionary<FieldExtra, object> Extras { get; }
        protected List<Field> Children { get; }

        private readonly PropertyType _propertyType;
        private readonly ControlType _controlType;

        private string _description;
        private string _title;

        protected FieldBuilder(string propertyName, T2 propertyValue, PropertyType propertyType, ControlType controlType)
        {
            PropertyName = propertyName;
            PropertyValue = propertyValue;
            Options = new Dictionary<FieldOption, object>();
            Constraints = new Dictionary<FieldConstraint, object>();
            Extras = new Dictionary<FieldExtra, object>();
            Children = new List<Field>();

            _propertyType = propertyType;
            _controlType = controlType;
        }

        public T1 Title(string title)
        {
            _title = title;
            return (T1) this;
        }

        public T1 Description(string description)
        {
            _description = description;
            return (T1) this;
        }

        public override Field Build(Action<string, object> action)
        {
            action(PropertyName, PropertyValue);
            var property = new Field(PropertyName, _title, _description, _propertyType, _controlType, Options, Constraints, Extras, Children);
            return property;
        }
    }
}