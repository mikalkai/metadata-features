﻿namespace Features
{
    public enum FieldExtra
    {
        /// <summary>
        /// Template for a new item created i.e. in a list.
        /// </summary>
        FieldTemplate,

        /// <summary>
        /// Name of the property where the item selection in a list is stored.
        /// </summary>
        IsSelectedProperty
    }
}