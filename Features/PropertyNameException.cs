﻿using System;

namespace Features
{
    public class PropertyNameException : Exception
    {
        public PropertyNameException(string propertyChain)
            : base($"Property chain '{propertyChain}' is too long. Only a single property is allowed in a chain after the parent.")
        {
        }
    }
}