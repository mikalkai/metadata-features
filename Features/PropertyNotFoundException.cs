﻿using System;

namespace Features
{
    public class PropertyNotFoundException : Exception
    {
        public PropertyNotFoundException(string propertyName, Type type)
            : base($"Property '{propertyName}' could not be found from type '{type.FullName}'.")
        {
        }
    }
}