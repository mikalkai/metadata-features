﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Features
{
    public static class PropertyNameResolver
    {
        public static string GetPropertyName<T>(this Expression<Func<T>> expression)
        {
            return (expression as LambdaExpression).GetPropertyName();
        }

        public static string GetPropertyName(this LambdaExpression lambda)
        {
            if (lambda == null)
            {
                return "undefined";
            }

            var memberExpr = AsMemberExpression(lambda.Body);
            if (memberExpr == null)
            {
                return "undefined";
            }

            var properties = new List<string>();
            while (memberExpr != null)
            {
                properties.Insert(0, memberExpr.Member.Name);
                memberExpr = AsMemberExpression(memberExpr.Expression);
            }

            if (properties.Count != 2)
            {
                throw new PropertyNameException(string.Join(".", properties));
            }

            return string.Join(".", properties.Skip(1));
        }

        private static MemberExpression AsMemberExpression(this Expression expression)
        {
            if (expression.NodeType == ExpressionType.Convert)
            {
                return ((UnaryExpression)expression).Operand as MemberExpression;
            }
            if (expression.NodeType == ExpressionType.MemberAccess)
            {
                return expression as MemberExpression;
            }
            return null;
        }
    }
}