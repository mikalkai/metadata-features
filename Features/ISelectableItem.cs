﻿namespace Features
{
    public interface ISelectableItem
    {
        bool IsSelected { get; set; }
    }
}