﻿namespace Features
{
    public enum ControlType
    {
        /// <summary>
        /// TextBox control.
        /// </summary>
        TextBox,

        /// <summary>
        /// CheckBox control.
        /// </summary>
        CheckBox,

        /// <summary>
        /// Single RadioButton control.
        /// </summary>
        RadioButton,

        /// <summary>
        /// ListBox control. 
        /// </summary>
        ListBox,

        /// <summary>
        /// Selection list box.
        /// </summary>
        SelectionListBox,

        /// <summary>
        /// TimePicker control.
        /// </summary>
        TimePicker,

        /// <summary>
        /// DatePicker control.
        /// </summary>
        DatePicker,

        /// <summary>
        /// DateTimePickerBuilder control.
        /// </summary>
        DateTimePicker,

        /// <summary>
        /// FieldGroup control.
        /// </summary>
        FieldGroup,
    }
}