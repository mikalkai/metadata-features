﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace Features
{
    public class Field : JObject
    {
        public Field(string name,
            string title,
            string description,
            PropertyType type,
            ControlType control,
            Dictionary<FieldOption, object> options,
            Dictionary<FieldConstraint, object> constraints,
            Dictionary<FieldExtra, object> extras,
            IEnumerable<Field> children)
        {
            this["@Name"] = name;
            this["@Title"] = title;
            this["@Description"] = description;
            this["@Type"] = type.ToString();
            this["@Control"] = control.ToString();
            this["@Options"] = CreateObject(options);
            this["@Constraints"] = CreateObject(constraints);
            this["@Extras"] = CreateObject(extras);
            CreateChildObjects(this, children);
        }

        public string Name => this["@Name"].Value<string>();

        private static void CreateChildObjects(JToken jObject, IEnumerable<Field> children)
        {
            foreach (var child in children)
            {
                jObject[child.Name] = child;
            }
        }

        private static JObject CreateObject(object obj)
        {
            return FromObject(obj,
                JsonSerializer.CreateDefault(new JsonSerializerSettings
                {
                    Converters = new List<JsonConverter>
                    {
                        new StringEnumConverter(),
                        new IsoDateTimeConverter()
                    },
                    ContractResolver = new ReservedNameContractResolver()
                }));
        }
    }
}