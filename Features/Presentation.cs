﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Features
{
    public class Presentation : JObject
    {
        public Presentation(IEnumerable<Field> fields)
        {
            ToJson(this, fields);
        }

        private static void ToJson(JObject jObject, IEnumerable<Field> fields)
        {
            foreach (var field in fields)
            {
                jObject[field.Name] = field;
            }
        }
    }
}