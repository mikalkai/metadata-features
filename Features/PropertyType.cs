﻿using System;

namespace Features
{
    public enum PropertyType
    {
        /// <summary>
        /// Text property is bind with the <see cref="string"/> type.
        /// </summary>
        Text,

        /// <summary>
        /// Number property is bind with the <see cref="int"/> type. 
        /// </summary>
        Number,

        /// <summary>
        /// Number property is bind with the <see cref="bool"/> type. 
        /// </summary>
        Boolean,

        /// <summary>
        /// Time property is bind with the <see cref="DateTime"/> type. 
        /// </summary>
        Time,

        /// <summary>
        /// Time property is bind with the generic <see cref="List"/> type. 
        /// </summary>
        List,

        /// <summary>
        /// Object property is bind with reference type. 
        /// </summary>
        Object,
    }
}